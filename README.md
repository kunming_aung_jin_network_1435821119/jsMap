# jsMap

#### 项目介绍
这是一个功能丰富的 jQuery 地图插件，采用 SVG 技术清晰的展示了中国地图，它拥有非常全面的参数接口，可以让开发者根据实际需求灵活配置。

网站地址：(http://jsmap.applinzi.com/)

#### 兼容情况

兼容支持 SVG 的浏览器。

#### 如何使用

1. 引入 jQuery 文件；
2. 引入 jsMap 文件；
3. 通过 jsMap.config 配置地图即可。

#### 程序 demo

```
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>jsMap Demo</title>
        <script src="jquery.min.js"></script>
        <script src="jsMap.min.js"></script>
        <script>
            $(function () {
                jsMap.config("#container", {
                    name: "china",
                    width: 900,
                    height: 500
                });
            })
        </script>
    </head>
    <body>
	    
        <div id="container"></div>

    </body>
</html>
```

#### 参数属性
| 属性          | 说明                                                                                                                     | 类型            | 默认值  |
|---------------|--------------------------------------------------------------------------------------------------------------------------|-----------------|---------|
| name          | 地图名称，设置 china 则绘制中国地图，若要设置省份的地图，需要传入该省份的汉语全拼（注意：山西是 shanxi，陕西是 shaanxi） | String          | china   |
| width         | 地图容器的宽度                                                                                                           | Number          | 800     |
| height        | 地图容器的高度                                                                                                           | Number          | 500     |
| strokeColor   | 描边颜色                                                                                                                 | String          | #fff    |
| strokeWidth   | 描边宽度                                                                                                                 | Number          | 1       |
| fill          | 填充颜色                                                                                                                 | String / Object | #3f99f9 |
| hoverFill     | 鼠标悬浮时的填充颜色                                                                                                     | String / Object | #fff11c |
| areaName      | 是否显示地区名称，可选值 true / false                                                                                    | Boolean         | false   |
| areaNameColor | 地区名称的文字颜色                                                                                                       | String          | #000    |
| hoverTip       | 鼠标悬浮时的内容展示                                                         | Boolean / Function | 当前地区名称 |
| hoverCallback  | 鼠标悬浮时的回调函数，参数分别是：id 和 name                                 | Function           | 空函数       |
| clickCallback  | 鼠标点击时的回调函数，参数分别是：id 和 name                                 | Function           | 空函数       |
| clickHighLight | 鼠标点击时高亮颜色，true 则使用默认颜色，false 无高亮效果，String 自定义颜色 | Boolean / String   | #dd2d01      |

